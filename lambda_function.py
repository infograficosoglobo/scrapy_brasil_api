import sys
import os
from utils import *

#utf-8 no console:
sys.stdout.reconfigure(encoding='utf-8')

URL_CASO = 'https://brasil.io/api/dataset/covid19/caso/data/?format=json&place_type=city&is_last=true'
URL_BRASIL = 'https://brasil.io/api/dataset/covid19/caso/data/?format=json&is_last=true&place_type=state'


def caso():
    mn_list = []
    total_results = {}
    pos = 0

    timer('Coleta de dados','Coletando resultados da primeira página de municípios')
    mn_caso, msg, status = get_data_mn(URL_CASO)
    
    for i in range(len(mn_caso['results'])):
        mn_list.insert(i + pos, mn_caso['results'][i])

    timer('Coleta de dados', 'Verificando paginação')
    while mn_caso['next'] is not None:
        timer('Coleta de dados', mn_caso['next'])
        mn_caso, msg, status = get_data_mn(mn_caso['next'])
        # Guarda a página de resultados atual
        target_index = len(mn_list)
        for i in range(len(mn_caso['results'])):
            mn_list.insert(i + target_index, mn_caso['results'][i])

    timer('Coleta de dados','Populando dict')
    total_results['all'] = mn_list

    #Cria os 3 tipos de arquivos (json,xml e csv)
    key = os.path.join(os.getcwd())

    timer('Coleta de dados','Criando json municípios')
    json_results = create_file(key, 'mn_all.json', total_results)

    xml_results = json2xml(total_results)
    timer('Coleta de dados','Criando xml municípios')
    status_xml = create_xml_file(key, 'mn_all.xml', xml_results)
    if status_xml:
        xml_format = normalize_xml('mn_all.xml')
        
    timer('Coleta de dados','Criando csv municípios')
    csv_results = json2csv('mn_all',total_results)

def brasil():
    total_results = {}

    brasil, msg, status = get_data_mn(URL_BRASIL)

    timer('Coleta de dados', 'Dados resgatados, populando dict')
    total_results['all'] = brasil['results']

    #Cria os 3 tipos de arquivos (json,xml e csv)
    key = os.path.join(os.getcwd())

    timer('Coleta de dados','Criando json Brasil')
    json_results = create_file(key, 'br_all.json', total_results)

    xml_results = json2xml(total_results)
   
    timer('Coleta de dados','Criando xml Brasil')
    status_xml = create_xml_file(key, 'br_all.xml', xml_results)
    if status_xml:
        xml_format = normalize_xml('br_all.xml')

    timer('Coleta de dados','Criando csv Brasil')
    csv_results = json2csv('br_all',total_results)


def main():
    timer('Iniciando Script', 'Coleta de dados MN, output em json/csv/xml')
    caso()

    timer('Iniciando Script', 'Coleta de dados BR, output em json/csv/xml')
    brasil()

if __name__ == "__main__":
    main()