# Crawler municípios brasil.io

Pega a listagem de todos os municípios da API brasil.io e cria arquivos (json,xml,csv) contendo os dados.

## Installation

[pip](https://pip.pypa.io/en/stable/).

```bash
pip install -r requirements.txt
```

## Usage

```bash
python lambda_function.py
```