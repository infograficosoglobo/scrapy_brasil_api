from xml.etree import ElementTree as obj
import requests
import random
import json
import time
import csv
import os

def timer(operation, description):
    ts = time.gmtime()
    print("["+str(time.strftime("%Y-%m-%d %H:%M:%S", ts))+"] ["+operation+"] - "+description)

class Downloader(object):
    def __init__(self, use_proxy=False):
        self.user_agents = [
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like '
            'Gecko) Chrome/36.0.1985.143 Safari/537.36',
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6)"
            " AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/68.0.3440.106 Safari/537.36",
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) '
            'Gecko/20100101 '
            'Firefox/55.0'
        ]
        self.headers = {'User-Agent': self.get_user_agent()}
        self.use_proxy = use_proxy
        self.PROXY_INFOGLOBO = None

    def get_user_agent(self):
        return random.choice(self.user_agents)

    def set_user_agent(self, user_agent):
        if type(user_agent) == list:
            self.user_agents = list(set(self.user_agents + user_agent))
        else:
            self.user_agents.append(user_agent)

    def do_request(self, url):
        if self.use_proxy:
            r = requests.get(
                url, proxies=self.PROXY_INFOGLOBO, headers=self.headers)
        else:
            r = requests.get(url, headers=self.headers)
        return r

    def request_data(self, url, data_format=None):
        data = None
        status = True
        msg = "[Downloader] Download dos dados realizado com sucesso."
        r = self.do_request(url)
        r.encoding = 'UTF-8'
        if r.status_code == 200:
            if data_format == 'json':
                data = r.json()
            elif data_format == 'bytes':
                data = r.content
            else:
                r.encoding = r.apparent_encoding
                data = r.text
        else:
            msg = f"[Downloader] Erro ao consultar os dados da url: {url} " \
                  f"informada. Code: {str(r.status_code)}"
            status = False
        return data, msg, status

    def request_file(self, url):
        status = False
        msg = None
        r = self.do_request(url)
        if r.status_code == 200:
            status = True
        else:
            msg = f"[Downloader] Erro ao consultar os dados da url: {url} " \
                  f"informada. Code: {str(r.status_code)}"
        return r, msg, status

def create_dir(path_tmp):
    if not os.path.exists(path_tmp):
        os.makedirs(path_tmp)

def create_file(path, filename, data):
    status = False
    create_dir(path)
    try:
        with open(filename, "w") as _file:
            _file.write(json.dumps(data))
        _file.close()
        status = True
        timer('Coleta de dados', f'Arquivo {filename} criado com sucesso!')
    except Exception as e:
        timer('Coleta de dados', f'Falha ao criar o arquivo {filename}.')
    return status

def create_xml_file(path, filename, data):
    status = False
    create_dir(path)
    try:
        with open(filename, "w", encoding='utf-8') as _file:
            _file.write(data)
        _file.close()
        status = True
        timer('Coleta de dados', f'Arquivo {filename} criado com sucesso!')
    except Exception as e:
        timer('Coleta de dados', f'Falha ao criar o arquivo {filename}.')
    return status

def get_data_mn(url):
    content = None
    msg = None
    status = None
    downloader = Downloader()
    content, msg, status = downloader.request_data(url, 'json')
    return content, msg, status

def json2xml(json_obj, line_padding=""):
    result_list = list()

    json_obj_type = type(json_obj)

    if json_obj_type is list:
        for sub_elem in json_obj:
            result_list.append(json2xml(sub_elem, line_padding))

        return "\n".join(result_list)

    if json_obj_type is dict:
        for tag_name in json_obj:
            sub_obj = json_obj[tag_name]
            result_list.append("%s<%s>" % (line_padding, tag_name))
            result_list.append(json2xml(sub_obj, "\t" + line_padding))
            result_list.append("%s</%s>" % (line_padding, tag_name))

        return "\n".join(result_list)

    return "%s%s" % (line_padding, json_obj)

def json2csv(filename, json_obj):
    mn_data = json_obj['all'] 
    
    # Abrindo arquivo para escrita 
    data_file = open(filename+'.csv', 'w', encoding='utf-8') 
    
    # Criando objeto csv writer 
    csv_writer = csv.writer(data_file) 
    
    count = 0
    
    for mn in mn_data: 
        if count == 0: 
    
            # Escrevendo os headers 
            header = mn.keys() 
            csv_writer.writerow(header) 
            count += 1
    
        # Escrevendo o csv 
        csv_writer.writerow(mn.values()) 
    data_file.close()

def normalize_xml(filename):
    tree = obj.ElementTree(file=filename)
    root = tree.getroot()

    tree = obj.ElementTree(root)
    tree.write(filename, encoding='utf-8', xml_declaration=True)